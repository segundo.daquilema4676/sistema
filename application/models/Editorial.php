<?php
class Editorial extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }
  // Insertar nuevas revistas
  function insertar($datos){
    $respuesta=$this->db->insert("editorial",$datos);
    return $respuesta;
  }
  // Consultar todas las revistas
  function consultarTodos(){
    $editoriales=$this->db->get("editorial");
    if ($editoriales->num_rows()>0){
      return $editoriales->result();
    } else {
      return false;
    }
  }

  // Eliminar revista por id
  function eliminar($id){
    $this->db->where("id",$id);
    return $this->db->delete("editorial");
  }
  // Consultar una única revista por id
  function obtenerPorId($id){
    $this->db->where("id",$id);
    $editorial=$this->db->get("editorial");
    if ($editorial->num_rows()>0) {
      return $editorial->row();
    } else {
      return false;
    }
  }


  // Función para actualizar revistas
  function actualizar($id,$datos){
    $this->db->where("id",$id);
    return $this->db
                ->update("editorial",$datos);
  }

} // Fin de la clase
?>
