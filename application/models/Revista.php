<?php
class Revista extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }

  // Insertar nueva Revista
  function insertar($datos){
    $respuesta = $this->db->insert("revista", $datos);
    return $respuesta;
  }

  // Consultar todas las revistas
  function consultarTodos()
  {
    $revistas = $this->db->get("revista");
    if ($revistas->num_rows() > 0) {
        return $revistas->result();
    } else {
        return false;
    }
  }

  // Eliminar revista por id
  function eliminar($id){
    $this->db->where("id", $id);
    return $this->db->delete("revista");
  }

  // Obtener revista por id
  function obtenerPorId($id){
    $this->db->where("id", $id);
    $query = $this->db->get("revista");

    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }

  // Actualizar revista
  function actualizar($id, $datos){
    $this->db->where("id", $id);
    return $this->db->update("revista", $datos);
  }

} // Fin de la clase
?>
