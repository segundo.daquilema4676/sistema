<?php
class Autores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Autor");
        $this->load->model("Investigador");
        $this->load->model("Articulo");

        // Deshabilitar errores y advertencias de PHP
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoAutores"] = $this->Autor->consultarTodos();
        $datos["listadoInvestigadores"] = $this->Investigador->consultarTodos();
        $datos["listadoArticulos"] = $this->Articulo->consultarTodos();
        $this->load->view("header");
        $this->load->view("autores/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idAutor)
    {
        $this->Autor->eliminar($idAutor);
        $this->session->set_flashdata("confirmacion", "Autor eliminado correctamente");
        redirect("autores/index");
    }

    public function nuevo()
    {
        $data["investigadores"] = $this->Investigador->consultarTodos();
        $data["articulos"] = $this->Articulo->consultarTodos();
        $this->load->view("header");
        $this->load->view("autores/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarAutor()
    {
        $datosNuevoAutor = array(
            "investigador_id" => $this->input->post("investigador_id"),
            "articulo_id" => $this->input->post("articulo_id"),
        );

        $this->Autor->insertar($datosNuevoAutor);
        $this->session->set_flashdata("confirmacion", "Autor guardado exitosamente");
        redirect('autores/index');
    }

    public function editar($idAutor)
    {
        $data["autorEditar"] = $this->Autor->obtenerPorId($idAutor);
        $data["investigadores"] = $this->Investigador->consultarTodos();
        $data["articulos"] = $this->Articulo->consultarTodos();
        $this->load->view("header");
        $this->load->view("autores/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarAutor()
    {
        $idAutor = $this->input->post("idAutor");

        $datosAutor = array(
            "investigador_id" => $this->input->post("investigador_id"),
            "articulo_id" => $this->input->post("articulo_id"),
        );

        $this->Autor->actualizar($idAutor, $datosAutor);
        $this->session->set_flashdata("confirmacion", "Autor actualizado exitosamente");
        redirect('autores/index');
    }

}
?>
