<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nuevo Financiador</title>
  <!-- Agrega referencias a jQuery y jQuery Validate -->
</head>
<body>
<h1>
  <br>
  <b>
    <i class="fa fa-plus-circle"></i>
    Nuevo Financiador
  </b>
</h1>
<br>

<form class="" action="<?php echo site_url('financiadores/guardarFin'); ?>" enctype="multipart/form-data" method="post" id="frm_nuevo_volumen">
<br>
  <label for=""><b>Nombre del Financiador: </b></label>
  <input type="text" name="nombre" id="nombre" class="form-control" oninput="soloLetras(this)" value="" required placeholder="Ingrese el nombre del financiador">
  <br>
  <label for="id"><b>Revista: </b></label>
<select name="revista_id" id="revista_id" class="form-control" required>
    <option value="">Seleccione la Revista</option>
    <?php foreach ($listadoRevistas as $revista) : ?>
        <?php $selected = ($revista->id == $selectedRevistaid) ? 'selected' : ''; ?>
        <option value="<?php echo $revista->id; ?>" <?php echo $selected; ?>><?php echo $revista->nombre; ?></option>
    <?php endforeach; ?>
</select>

 <br>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">
        <i class="fa fa-cloud-upload fa-fade" aria-hidden="true"></i>
        Guardar
      </button>
      <a href="<?php echo site_url('financiadores/indexFin'); ?>" class="btn btn-danger" >
        <i class="fa-solid fa-ban fa-fade"  > </i>
        Cancelar
      </a>
    </div>
  </div>

  <br><br>

</form>

<script type="text/javascript">
        $("#frm_nuevo_volumen").validate({
      errorClass: 'text-danger',
      rules: {
          "id": {
              required: true
          },
          "titulo": {
              required: true,
              minlength: 3,
              maxlength: 50
          }
      },
      messages: {
          "id": {
              required: '<span class="text-danger">Por favor, seleccione la revista</span>'
          },
          "titulo": {
              required: '<span class="text-danger">Debe ingresar el nombre</span>',
              minlength: '<span class="text-danger">El nombre debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">El nombre debe tener como máximo 50 caracteres</span>'
          }
      }
      });

</script>

<script type="text/javascript">
    function soloNumeros(input) {
        // Reemplaza cualquier carácter que no sea un dígito numérico o un espacio con una cadena vacía
        input.value = input.value.replace(/[^\d\s]/g, '');
    }
</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
