<h1>EDITAR COLABORADOR</h1>
<form class="" method="post" action="<?php echo site_url('colaboradores/actualizarcolaborador'); ?>" id="frm_nuevo_colaborador">
	<input type="hidden" name="id" id="id" value="<?php echo $colaboradorEditar->id; ?>">
	<label for=""><b>TIPO REVISTA: </b></label>
<select data-live-search="true" data-live-search-style="startsWith" class="selectpicker form-control" name="id" id="id" required>
    <option value="">Seleccione la Revista</option>
    <?php foreach ($listadoRevistas as $revista) : ?>
        <?php if ($revista->id_ == $colaboradorEditar->id) : ?>
            <option value="<?php echo $revista->id; ?>" selected><?php echo $revista->nombre; ?></option>
        <?php else: ?>
            <option value="<?php echo $revista->id; ?>"><?php echo $revista->nombre; ?></option>
        <?php endif; ?>
    <?php endforeach; ?>
</select>

<label for=""><b>TIPO ARTICULO: </b></label>
<select data-live-search="true" data-live-search-style="startsWith" class="selectpicker form-control" name="id" id="id" required>
    <option value="">Seleccione el articulo</option>
    <?php foreach ($listadoArticulos as $articulo) : ?>
        <?php if ($articulo->id_ == $colaboradorEditar->id) : ?>
            <option value="<?php echo $articulo->id; ?>" selected><?php echo $articulo->nombre; ?></option>
        <?php else: ?>
            <option value="<?php echo $articulo->id; ?>"><?php echo $articulo->nombre; ?></option>
        <?php endif; ?>
    <?php endforeach; ?>
</select>

<br>
  <label for="">
    <b>Nombre:</b>
  </label>
  <input type="text" name="nombre" id="nombre"
	value="<?php echo $colaboradorEditar->nombre; ?>"
  placeholder="Ingrese el nombre..." oninput="soloLetras(this)"class="form-control" required>
  <br>
  <label for="">
    <b>apellido:</b>
  </label>
  <input type="text" name="apellido" id="apellido"
	value="<?php echo $colaboradorEditar->apellido; ?>"
  placeholder="Ingrese el apellido..." oninput="soloLetras(this)" class="form-control" required>

    <br>

    <label for="">
    <b>Institucion:</b>
  </label>
  <input type="text" name="institucion" id="institucion"
	value="<?php echo $colaboradorEditar->institucion; ?>"
    placeholder="Ingrese la institucion..." oninput="soloLetras(this)" class="form-control" required>

   <br>       

    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('colaboradores/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-
spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>

<script type="text/javascript">
        $("#frm_nuevo_colaborador").validate({
      errorClass: 'text-danger',
      rules: {
          "id": {
              required: true
          },
          "nombre": {
              required: true,
              minlength: 3,
              maxlength: 50
          },
          "apellido": {
              required: true,
              minlength: 3,
              maxlength: 50
          },
          "institucion": {
              required: true,
              minlength: 3,
              maxlength: 50
          }
      },
      messages: {
          "id": {
              required: '<span class="text-danger">Por favor, seleccione el banco</span>'
          },
          "nombre": {
              required: '<span class="text-danger">Debe ingresar el nombre</span>',
              minlength: '<span class="text-danger">El nombre debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">El nombre debe tener como máximo 50 caracteres</span>'
          },
          "apellido": {
              required: '<span class="text-danger">Debe ingresar el apellido exacta</span>',
              minlength: '<span class="text-danger">El apellido debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">El apellido debe tener como máximo 50 caracteres</span>'
          },
          "institucion": {
              required: '<span class="text-danger">Debe ingresar la institucion de donde viene</span>',
              minlength: '<span class="text-danger">La institucion debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">La institucion debe tener como máximo 50 caracteres</span>'
          }
      }
      });

</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
