<?php
class Evaluador extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Insertar nuevos
    function insertar($datos)
    {
        $respuesta = $this->db->insert("evaluadores", $datos);
        return $respuesta;
    }

    // Consulta de todos
    function consultarTodos()
    {
        $evaluadores = $this->db->get("evaluadores");
        if ($evaluadores->num_rows()>0) {
            return $evaluadores->result();
        } else {
            return false;
        }
    }

    // Eliminar por ID
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("evaluadores");
    }

    // Consultar un solo dato por ID
    function obtenerPorId($id)
    {
        $this->db->where("id", $id);
        $evaluador = $this->db->get("evaluadores");
        if ($evaluador->num_rows() > 0) {
            return $evaluador->row();
        } else {
            return false;
        }
    }

    // Actualizar datos
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("evaluador", $datos);
    }

    // Obtener revista
    function obtenerRevista()
    {
        $rivistas = $this->db->get("revista")->result();
        return $revistas;
    }
}
?>
