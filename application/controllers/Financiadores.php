<?php
class Financiadores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Financiador");
        $this->load->model("Revista");
    }

    public function indexFin()
    {
        $data['listadoRevistas'] = $this->Revista->consultarTodos();

        $data["listadoFinanciadores"] = $this->Financiador->consultarTodos();

        $this->load->view("header");
        $this->load->view("financiadores/indexFin", $data);
        $this->load->view("footer");
    }

    // Eliminacion de Financiador
    public function borrar($id)
    {
        $this->Financiador->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Financiador eliminado exitosamente");
        redirect("financiadores/indexFin");
    }

    // Renderizacion del formulario de nuevo 
    public function nuevoFin()
    {
        $data['listadoRevistas'] = $this->Revista->consultarTodos();

        $this->load->view("header");
        $this->load->view("financiadores/nuevoFin", $data);
        $this->load->view("footer");
    }

    // Capturando datos e insertando un nuevo financiador
    public function guardarFin()
    {
        $id = $this->input->post("id");

        $datosNuevoFinanciador = array(
            "nombre" => $this->input->post("nombre"),
        );

        $this->Financiador->insertar($datosNuevoFinanciador);
        $this->session->set_flashdata("confirmacion", "Financiador guardado exitosamente");
        redirect('financiadores/indexFin');
    }

      public function editFin($id)
      {
          // Obtener los datos del corresponsal a editar
          $data["financiadorEditar"] = $this->Financiador->obtenerPorId($id);
          $data['listadoRevistas'] = $this->Revista->consultarTodos();

          $this->load->view("header");
          $this->load->view("financiadores/editFin", $data);
          $this->load->view("footer");
      }

    public function actualizarFin()
    {
        $id = $this->input->post("id");

        $datosFinanciador = array(

            "nombre" => $this->input->post("nombre"),

        );

        $this->Financiador->actualizar($id, $datosFinanciador);
        $this->session->set_flashdata("confirmacion", "Financiador actualizado exitosamente");
        redirect('financiadores/indexFin');
    }
}
?>
