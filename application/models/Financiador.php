<?php
class Financiador extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Insertar nuevos financiadores
    function insertar($datos)
    {
        $respuesta = $this->db->insert("financiadores", $datos);
        return $respuesta;
    }

    // Consulta de todos
    function consultarTodos()
    {
        $financiadores = $this->db->get("financiadores");
        if ($financiadores->num_rows() > 0) {
            return $financiadores->result();
        } else {
            return false;
        }
    }

    // Eliminar por ID
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("financiadores");
    }

    // Consultar un solo dato por ID
    function obtenerPorId($id)
    {
        $this->db->where("id", $id);
        $financiador = $this->db->get("financiadores");
        if ($financiador->num_rows() > 0) {
            return $financiador->row();
        } else {
            return false;
        }
    }

    // Actualizar datos
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("financiadores", $datos);
    }

    // Obtener revista
    function obtenerRevista()
    {
        $rivistas = $this->db->get("revista")->result();
        return $revistas;
    }
}
?>