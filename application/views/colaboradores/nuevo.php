<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nuevo Colaborador</title>
  <!-- Agrega referencias a jQuery y jQuery Validate -->
</head>
<body>
<h1>
  <br>
  <b>
    <i class="fa fa-plus-circle"></i>
    Nuevo Colaborador
  </b>
</h1>
<br>

<form class="" action="<?php echo site_url('colaboradores/guardarColaborador'); ?>" enctype="multipart/form-data" method="post" id="frm_nuevo_colaborador">
  <label for=""><b>TIPO REVISTA: </b></label>
    <select name="id" id="id" class="form-control" required>
      <option value="">Seleccione la Revista</option>
      <?php foreach ($listadoRevistas as $revista) : ?>
          <option value="<?php echo $revista->id; ?>"><?php echo $revista->nombre; ?></option>
      <?php endforeach; ?>
    </select>
  <br>
  <label for=""><b>TIPO ARTICULO: </b></label>
    <select name="id" id="id" class="form-control" required>
      <option value="">Seleccione el articulo</option>
      <?php foreach ($listadoArticulos as $articulo) : ?>
          <option value="<?php echo $articulo->id; ?>"><?php echo $articulo->nombre; ?></option>
      <?php endforeach; ?>
    </select>
    <br>
  <label for=""><b>NOMBRE: </b></label>
  <input type="text" name="nombre" id="nombre" class="form-control" oninput="soloLetras(this)" value="" required placeholder="Ingrese el nombre">
  <br>

  <label for=""><b>APELLIDO: </b></label>
  <input type="text" name="apelldio" id="apellido" class="form-control" oninput="soloLetras(this)" value="" required placeholder="Ingrese el apellido">

  <br>
  
  <label for=""><b>INSTITUCION: </b></label>
  <input type="text" name="institucion" id="institucion" class="form-control" oninput="soloLetras(this)" value="" required placeholder="Ingrese la institucion">
  <br>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">
        <i class="fa fa-cloud-upload fa-fade" aria-hidden="true"></i>
        Guardar
      </button>
      <a href="<?php echo site_url('colaboradores/index'); ?>" class="btn btn-danger" >
        <i class="fa-solid fa-ban fa-fade"  > </i>
        Cancelar
      </a>
    </div>
  </div>

  <br><br>

</form>

<script type="text/javascript">
        $("#frm_nuevo_colaborador").validate({
      errorClass: 'text-danger',
      rules: {
          "id": {
              required: true
          },
          "nombre": {
              required: true,
              minlength: 3,
              maxlength: 50
          },
          "apellido": {
              required: true,
              minlength: 3,
              maxlength: 50
          },
          "institucion": {
              required: true,
              minlength: 3,
              maxlength: 50
          }
      },
      messages: {
          "id": {
              required: '<span class="text-danger">Por favor, seleccione la revista</span>'
          },
          "nombre": {
              required: '<span class="text-danger">Debe ingresar el nombre</span>',
              minlength: '<span class="text-danger">El nombre debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">El nombre debe tener como máximo 50 caracteres</span>'
          },
          "apellido": {
              required: '<span class="text-danger">Debe ingresar el apellido exacta</span>',
              minlength: '<span class="text-danger">El apellido debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">El apellido debe tener como máximo 50 caracteres</span>'
          },
          "institucion": {
              required: '<span class="text-danger">Debe ingresar la institucion de donde viene</span>',
              minlength: '<span class="text-danger">La institucion debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">La institucion debe tener como máximo 50 caracteres</span>'
          }
      }
      });

</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
