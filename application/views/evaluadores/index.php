<br>
<i class="fas fa-revista fa-2x"> Evaluador</i>
<div class="row">
  <div class="col-md-12 text-end">
    <!-- Button trigger modal -->

    <a href="<?php echo site_url('evaluadores/nuevo'); ?>"class="btn btn-outline-success">
      <i class="fa fa-plus-circle fa-1x"></i>
      Agregar Evaluador
    </a>
    <br><br>
  </div>
  <!-- Google Maps API -->
</div>
<?php if ($listadoEvaluadores): ?>
    <table class="table table-bordered" id="tbl_evaluadores">
        <thead>
              <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>INSTITUCIÓN</th>
                <th>ARTICULO</th>
                <th>REVISTA</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoEvaluadores as $evaluador): ?>
                <tr>
                  <td><?php echo $evaluador->id; ?></td>
                  <td><?php echo $evaluador->nombre; ?></td>
                  <td><?php echo $evaluador->apellido; ?></td>
                  <td><?php echo $evaluador->institucion; ?></td>
                  <td>
                      <?php
                      // Obtener el nombre deL articulo correspondiente al id del evaluador
                      $nombre_articulo = '';
                      foreach ($listadoArticulos as $articulo) {
                          if ($articulo->id == $articulo->id) {
                              $nombre_articulo = $articulo->nombre;
                              break;
                          }
                      }
                      echo $nombre_articulo;
                      ?>
                  </td>

                  <td>
                      <?php
                      // Obtener el nombre de la revista correspondiente al id del evaluador
                      $nombre_revista = '';
                      foreach ($listadoRevistas as $revistas) {
                          if ($evaluador->id == $revista->id) {
                              $nombre_revista = $revista->nombre;
                              break;
                          }
                      }
                      echo $nombre_revista;
                      ?>
                  </td>

                  <td>

                    <a href="<?php echo site_url('evaluadores/editar/').$evaluador->id; ?>"
                        class="btn btn-warning"
                        title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="javascript:void(0);" onclick="borrar('<?php echo site_url('evaluadores/borrar/') . $evaluador->id; ?>')"
                       class="btn btn-danger"
                       title="Borrar">
                       <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>



<!-- Modal -->

<script type="text/javascript">
        function borrar(url){
          iziToast.question({
              timeout: 15000,
              close: true,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Está seguro de eliminar el evaluador seleccionado?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {
                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=url;
                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
        }
      </script>


<?php else: ?>
  <div class="alert alert-danger">
      No se encontro evaluadores registrados
  </div>
<?php endif; ?>
<script type="text/javascript" >
    $('#tbl_evaluadores').DataTable( {
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
            },
            'print',
            'csv'
        ]
    } );
</script>
