<?php
class Editoriales extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Editorial");

        // Disable PHP errors and warnings
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoEditoriales"] = $this->Editorial->consultarTodos();
        $this->load->view("header");
        $this->load->view("editoriales/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idEditorial)
    {
        $this->Editorial->eliminar($idEditorial);
        $this->session->set_flashdata("confirmacion", "Editorial eliminada correctamente");

        redirect("editoriales/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("editoriales/nuevo");
        $this->load->view("footer");
    }

    public function guardarEditorial()
    {
        $datosNuevaEditorial = array(
            "nombre" => $this->input->post("nombre"),
            "ubicacion" => $this->input->post("ubicacion"),
            "revista" => $this->input->post("revista")
        );

        $this->Editorial->insertar($datosNuevaEditorial);
        $this->session->set_flashdata("confirmacion", "Editorial guardada exitosamente");
        redirect('editorial/index');
    }

    public function editar($idEditorial)
    {
        $data["editorialEditar"] = $this->Editorial->obtenerPorId($idEditorial);
        $this->load->view("header");
        $this->load->view("editoriales/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarEditorial()
    {
        $idEditorial = $this->input->post("idEditorial");

        $datosEditorial = array(
            "nombre" => $this->input->post("nombre"),
            "ubicacion" => $this->input->post("ubicacion"),
            "revista" => $this->input->post("revista")    
        );

        $this->Editorial->actualizar($idEditorial, $datosEditorial);
        $this->session->set_flashdata("confirmacion", "Editorial actualizada exitosamente");
        redirect('editoriales/index');
    }

}
?>
