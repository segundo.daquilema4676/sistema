<?php
class Autor extends CI_MODEL
{
  

    // Insertar nuevo autor
    function insertar($datos){
        $respuesta = $this->db->insert("autor", $datos);
        return $respuesta;
    }

    // Consultar todos los autores
    function consultarTodos()
    {
        $autores = $this->db->get("autor");
        if ($autores->num_rows() > 0) {
            return $autores->result();
        } else {
            return false;
        }
    }

    // Eliminar autor por id
    function eliminar($id){
        $this->db->where("id", $id);
        return $this->db->delete("autor");
    }

    // Obtener autor por id
    function obtenerPorId($id){
        $this->db->where("id", $id);
        $query = $this->db->get("autor");

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    // Actualizar autor
    function actualizar($id, $datos){
        $this->db->where("id", $id);
        return $this->db->update("autor", $datos);
    }
}
