<br>
<i class="fas fa-revista fa-2x"> Colaboradores</i>
<div class="row">
  <div class="col-md-12 text-end">
    <!-- Button trigger modal -->

    <a href="<?php echo site_url('colaboradores/nuevo'); ?>"class="btn btn-outline-success">
      <i class="fa fa-plus-circle fa-1x"></i>
      Agregar Colaborador
    </a>
    <br><br>
  </div>
  <!-- Google Maps API -->
</div>
<?php if ($listadoColaboradores): ?>
    <table class="table table-bordered" id="tbl_colaboradores">
        <thead>
              <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>INSTITUCION</th>
                <th>ARTICULO</th>
                <th>REVISTA</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoColaboradores as $colaborador): ?>
                <tr>
                  <td><?php echo $colaborador->id; ?></td>
                  <td><?php echo $colaborador->nombre; ?></td>
                  <td><?php echo $colaborador->apellido; ?></td>
                  <td><?php echo $colaborador->institucion; ?></td>
                  <td><?php echo $colaborador->articulo_id; ?></td>
                  <td><?php echo $colaborador->revista_id; ?></td>
                  <td>
                      <?php
                      // Obtener el nombre de la revista correspondiente al id del volumen
                      $nombre_colaborador = '';
                      foreach ($listadoColaborador as $colaborador) {
                          if ($colaborador->id == $colaborador->id) {
                              $nombre_colaborador = $colaborador->nombre;
                              break;
                          }
                      }
                      echo $nombre_colaborador;
                      ?>
                  </td>

                  <td>

                    <a href="<?php echo site_url('colaboradores/editar/').$colaborador->id; ?>"
                        class="btn btn-warning"
                        title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="javascript:void(0);" onclick="borrar('<?php echo site_url('colaboradores/borrar/') . $colaborador->id; ?>')"
                       class="btn btn-danger"
                       title="Borrar">
                       <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>



<!-- Modal -->

<script type="text/javascript">
        function borrar(url){
          iziToast.question({
              timeout: 15000,
              close: true,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Está seguro de eliminar el colaborador seleccionado?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {
                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=url;
                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
        }
      </script>


<?php else: ?>
  <div class="alert alert-danger">
      No se encontro colaboradores registrados
  </div>
<?php endif; ?>
<script type="text/javascript" >
    $('#tbl_colaboradores').DataTable( {
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
            },
            'print',
            'csv'
        ]
    } );
</script>
