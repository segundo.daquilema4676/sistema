<h1>
  <i class="fa fa-credit-card"></i>
  AUTORES
</h1>

<div class="row">
  <div class="col-md-12 text-end">

    <!-- Button trigger modal -->

    <a href="<?php echo site_url('autores/nuevo');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR AUTOR
    </a>
    <br><br>
  </div>
</div>
<?php if ($listadoAutores): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>INVESTIGADOR</th>
      <th>ARTICULO</th>
      <th>ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoAutores as $autor): ?>
    <tr>
      <td><?php echo $autor->id; ?></td>
      <td>
        <?php
        if ($autor  ->investigador_id) {
          $investigador = $this->Investigador->obtenerPorId($autor->investigador_id);
          echo $investigador->nombre;
        } else {
          echo 'N/A';
        }
        ?>
      </td>
      <td>
        <?php
        if ($autor->articulo_id) {
          $articulo = $this->Articulo->obtenerPorId($autor->articulo_id);
          echo $articulo->titulo;
        } else {
          echo 'N/A';
        }
        ?>
      </td>
      <td>
        <a href="<?php echo site_url('autores/editar/') . $autor->id; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>
        <a href="<?php echo site_url('autores/borrar/') . $autor->id; ?>" class="btn btn-danger" title="Eliminar">
          <i class="fa fa-trash"></i>
          Eliminar
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          <i class="fa fa-eye"></i> MAPA DE AUTORES
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </h5>
      <div class="modal-body">
        <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;">

          <script type="text/javascript">
            function initMap(){
              var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
              var miMapa = new google.maps.Map(
                document.getElementById('reporteMapa'),
                {
                  center: coordenadaCentral,
                  zoom: 8,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                }
              );
              <?php foreach ($listadoAutores as $autor): ?>
                // Agrega marcadores para cada autor en el mapa
                var coordenadaTemporal = new google.maps.LatLng(<?php echo $autor->latitud; ?>, <?php echo $autor->longitud; ?>);
                var marcador = new google.maps.Marker({
                  position: coordenadaTemporal,
                  map: miMapa,
                  title: 'Autor <?php echo $autor->id; ?>'
                });
              <?php endforeach; ?>
            }
          </script>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
<div class="alert alert-danger">
  No se encontraron autores registrados
</div>
<?php endif; ?>
