<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVA REVISTA
  </b>
</h1>

<div class="container">
  <div class="row">
    <div class="col-md-6">
      <form action="<?php echo site_url('revistas/guardarRevista');?>" method="post" enctype="multipart/form-data" id="formulario_revista">
        <div class="form-group">
          <label for="nombre">NOMBRE:</label>
          <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese el nombre">
        </div>
        <div class="form-group">
          <label for="autor">AUTOR:</label>
          <input type="text" name="autor" id="autor" class="form-control" placeholder="Ingrese el autor">
        </div>
        <div class="form-group">
          <label for="tipo_publicacion">TIPO DE PUBLICACIÓN:</label>
          <input type="text" name="tipo_publicacion" id="tipo_publicacion" class="form-control" placeholder="Ingrese el tipo de publicación">
        </div>
        <div class="form-group">
          <label for="resumen">RESUMEN:</label>
          <textarea name="resumen" id="resumen" class="form-control" placeholder="Ingrese el resumen"></textarea>
        </div>
        <div class="form-group">
          <label for="palabras_clave">PALABRAS CLAVE:</label>
          <textarea name="palabras_clave" id="palabras_clave" class="form-control" placeholder="Ingrese las palabras clave"></textarea>
        </div>

        <br>
        <div class="form-group">
          <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save"></i> GUARDAR</button>
          <a href="<?php echo site_url('revistas/index');?>" class="btn btn-danger"> <i class="fa fa-ban"></i> CANCELAR</a>
        </div>
      </form>
    </div>
    <div class="col-md-6">
      <img src="<?php echo base_url('static/img/nuevo.png'); ?>" alt="" style="width: 500px;">

    </div>
  </div>
</div>

<script type="text/javascript">
    $("#formulario_revista").validate({
        rules:{
            "nombre": {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            "autor": {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            "tipo_publicacion": {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            "resumen": {
                required: true
            },
            "palabras_clave": {
                required: true
            },
            "foto": {
                required: true
            }
        },
        messages:{
            "nombre": {
                required: "Por favor, ingrese el nombre de la revista",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "El nombre no puede tener más de 100 caracteres"
            },
            "autor": {
                required: "Por favor, ingrese el autor de la revista",
                minlength: "El autor debe tener al menos 2 caracteres",
                maxlength: "El autor no puede tener más de 100 caracteres"
            },
            "tipo_publicacion": {
                required: "Por favor, ingrese el tipo de publicación",
                minlength: "El tipo de publicación debe tener al menos 2 caracteres",
                maxlength: "El tipo de publicación no puede tener más de 50 caracteres"
            },
            "resumen": {
                required: "Por favor, ingrese el resumen de la revista"
            },
            "palabras_clave": {
                required: "Por favor, ingrese las palabras clave de la revista"
            },
            "foto": {
                required: "Por favor, seleccione una fotografía para la revista"
            }
        },
        errorClass: "text-danger"
    });
</script>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(-0.18522025692690522, -78.47587495002826);

    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Selecciona la ubicacion',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>
