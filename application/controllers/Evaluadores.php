<?php
class Evaluadores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Evaluador");
        $this->load->model("Revista");
        $this->load->model("Articulo");

    }

    public function index()
    {
        // Obtener todos los articulos
        $data['listadoRevistas'] = $this->Revista->consultarTodos();
        $data["listadoArticulos"] = $this->Articulo->consultarTodos();

        // Obtener todos los Volumenes
        $data["listadoEvaluadores"] = $this->Evaluador->consultarTodos();

        $this->load->view("header");
        $this->load->view("evaluadores/index", $data);
        $this->load->view("footer");
    }

    // Eliminacion de Volumenes
    public function borrar($id)
    {
        $this->Evaluador->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Evaluador eliminado exitosamente");
        redirect("evaluadores/index");
    }

    // Renderizacion del formulario de nuevo volumen
    public function nuevo()
    {
        // Obtener todos los articulos
        $data['listadoRevistas'] = $this->Revista->consultarTodos();
        $data["listadoArticulos"] = $this->Articulo->consultarTodos();

        $this->load->view("header");
        $this->load->view("evaluadores/nuevo", $data);
        $this->load->view("footer");
    }

    // Capturando datos e insertando un nuevo evaluador
    public function guardarEvaluador()
    {
        $id = $this->input->post("id");

        $datosNuevoEvaluador = array(
            "id" => $id,
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "institucion" => $this->input->post("institucion")
        );

        $this->Evaluador->insertar($datosNuevoEvaluador);
        $this->session->set_flashdata("confirmacion", "Evaluador guardado exitosamente");
        redirect('evaluadores/index');
    }

    // Renderizar el formulario de edicion
    // Renderizar el formulario de edicion
      public function editar($id)
      {
          // Obtener los datos del corresponsal a editar
          $data["evaluadorEditar"] = $this->Evaluador->obtenerPorId($id);

          // Obtener todos las agencias
          $data['listadoRevistas'] = $this->Revista->consultarTodos();

          // Cargar la vista de edición con los datos del corresponsal y las agencias
          $this->load->view("header");
          $this->load->view("evaluadores/editar", $data);
          $this->load->view("footer");
      }
    // Actualizar datos de volumen
    public function actualizarEvaluador()
    {
        $id = $this->input->post("id");
        $datosEvaluador = array(
          "nombre" => $this->input->post("nombre"),
          "apellido" => $this->input->post("apellido"),
          "institucion" => $this->input->post("institucion")
        );

        $this->Evaluador->actualizar($id, $datosEvaluador);
        $this->session->set_flashdata("confirmacion", "Evaluador actualizado exitosamente");
        redirect('evaluadores/index');
    }
}
?>
