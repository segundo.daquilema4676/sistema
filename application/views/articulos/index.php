<br>
<i class="fas fa-revista fa-2x"> Articulos</i>
<div class="row">
  <div class="col-md-12 text-end">
    <!-- Button trigger modal -->
    <a href="<?php echo site_url('articulos/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle fa-1x"></i>
      Agregar Articulo
    </a>
    <br><br>
  </div>
</div>

<?php if ($listadoArticulos): ?>
    <table class="table table-bordered" id="tbl_articulos">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>INSTITUCION</th>
                <th>ARTICULO</th>
                <th>REVISTA</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoArticulos as $articulo): ?>
                <tr>
                    <td><?php echo $articulo->id; ?></td>
                    <td><?php echo $articulo->titulo; ?></td>
                    <td><?php echo $articulo->resumen; ?></td>
                    <td><?php echo $articulo->fecha_publicacion; ?></td>
                    <td>
                        <?php 
                        // Verificar si $listadoRevistas está definido y no está vacío
                        if (isset($listadoRevistas) && !empty($listadoRevistas)) {
                            // Obtener el nombre de la revista asociada al artículo
                            foreach ($listadoRevistas as $revista) {
                                if ($revista->id == $articulo->revista_id) {
                                    echo $revista->nombre;
                                    break;
                                }
                            }
                        } else {
                            echo "Nombre de revista no disponible";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        // Verificar si $listadoVolumenes está definido y no está vacío
                        if (isset($listadoVolumenes) && !empty($listadoVolumenes)) {
                            // Obtener el nombre del volumen asociado al artículo
                            foreach ($listadoVolumenes as $volumen) {
                                if ($volumen->id == $articulo->volumen_id) {
                                    echo $volumen->nombre;
                                    break;
                                }
                            }
                        } else {
                            echo "Nombre de volumen no disponible";
                        }
                        ?>
                    </td>
                    <td>
                        <a href="<?php echo site_url('articulos/editar/') . $articulo->id; ?>" class="btn btn-warning" title="Editar"><i class="fa fa-pen"></i></a>
                        <a href="<?php echo site_url('articulos/borrar/') . $articulo->id; ?>" class="btn btn-danger" title="Eliminar" onclick="return confirm('¿Seguro desea eliminar este registro?');">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <div class="alert alert-danger">
        No se encontraron artículos registrados
    </div>
<?php endif; ?>

<!-- Modal -->
<script type="text/javascript">
    function borrar(url){
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar el artículo seleccionado?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }],
            ]
        });
    }
</script>

<script type="text/javascript" >
    $('#tbl_articulos').DataTable( {
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
            },
            'print',
            'csv'
        ]
    } );
</script>
