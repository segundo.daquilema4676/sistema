<?php
class Articulo extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }

  // Insertar nuevos artículos
  function insertar($datos){
    $respuesta=$this->db->insert("articulo",$datos);
    return $respuesta;
  }

  // Consultar todos los artículos
  function consultarTodos(){
    $articulos=$this->db->get("articulo");
    if ($articulos->num_rows()>0){
      return $articulos->result();
    } else {
      return false;
    }
  }

  // Eliminar artículo por id
  function eliminar($id){
    $this->db->where("id",$id);
    return $this->db->delete("articulo");
  }

  // Obtener artículo por id
  function obtenerPorId($id){
    $this->db->where("id",$id);
    $articulo=$this->db->get("articulo");
    if ($articulo->num_rows()>0) {
      return $articulo->row();
    } else {
      return false;
    }
  }

  // Actualizar artículo
  function actualizar($id,$datos){
    $this->db->where("id",$id);
    return $this->db->update("articulo",$datos);
  }

} // Fin de la clase

?>
