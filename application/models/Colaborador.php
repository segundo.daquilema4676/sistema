<?php
class Colaborador extends CI_MODEL
{

  function __construct()
  {
    parent::__construct();
  }
  // Insertar nuevas revistas
  function insertar($datos){
    $respuesta=$this->db->insert("colaboradores",$datos);
    return $respuesta;
  }
  // Consultar todas las revistas
  function consultarTodos(){
    $colaboradores=$this->db->get("colaboradores");
    if ($colaboradores->num_rows()>0){
      return $ecolaboradores->result();
    } else {
      return false;
    }
  }

  // Eliminar revista por id
  function eliminar($id){
    $this->db->where("id",$id);
    return $this->db->delete("colaboradores");
  }
  // Consultar una única revista por id
  function obtenerPorId($id){
    $this->db->where("id",$id);
    $colaborador=$this->db->get("colaboradores");
    if ($colaborador->num_rows()>0) {
      return $colaborador->row();
    } else {
      return false;
    }
  }


  // Función para actualizar revistas
  function actualizar($id,$datos){
    $this->db->where("id",$id);
    return $this->db
                ->update("colaboradores",$datos);
  }

} // Fin de la clase
?>
