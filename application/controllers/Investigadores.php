<?php
class Investigadores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Investigador");
    }

    public function index()
    {
        $data["listadoInvestigadores"] = $this->Investigador->consultarTodos();

        $this->load->view("header");
        $this->load->view("investigadores/index", $data);
        $this->load->view("footer");
    }
        public function borrar($id)
    {
        $this->Investigador->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Investigador eliminado exitosamente");
        redirect("investigadores/index");
    }

    // Renderizacion del formulario de nuevo Investigador
    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("investigadores/nuevo");
        $this->load->view("footer");
    }

    // Capturando datos e insertando un nuevo Investigador
    public function guardar()
    {
        $id = $this->input->post("id");

        $datosNuevoInvestigador = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "institucion" => $this->input->post("institucion"),
            "telefono" => $this->input->post("telefono"),
            "CI" => $this->input->post("CI"),
        );

        $this->Investigador->insertar($datosNuevoInvestigador);
        $this->session->set_flashdata("confirmacion", "Investigador guardado exitosamente");
        redirect('investigadores/index');
    }

  
      public function editar($id)
      {
          // Obtener los datos del corresponsal a editar
          $data["investigadorEditar"] = $this->Investigador->obtenerPorId($id);

          // Cargar la vista de edición con los datos del corresponsal y las agencias
          $this->load->view("header");
          $this->load->view("Investigadores/editar", $data);
          $this->load->view("footer");
      }
    // Actualizar datos de Investigador
    public function actualizar()
    {
        $id = $this->input->post("id");
        $datosInvestigador = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "institucion" => $this->input->post("institucion"),
            "telefono" => $this->input->post("telefono"),
            "CI" => $this->input->post("CI"),
        );

        $this->Investigador->actualizar($id, $datosInvestigador);
        $this->session->set_flashdata("confirmacion", "Investigador actualizado exitosamente");
        redirect('investigadores/index');
    }
}
?>
