<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nuevo Articulo</title>
  <!-- Agrega referencias a jQuery y jQuery Validate -->
</head>
<body>
<h1>
  <br>
  <b>
    <i class="fa fa-plus-circle"></i>
    Nuevo Articulo
  </b>
</h1>
<br>

<form class="" action="<?php echo site_url('articulos/guardar'); ?>" enctype="multipart/form-data" method="post" id="frm_nuevo_articulo">
<label for=""><b>TITULO: </b></label>
  <input type="text" name="titulo" id="titulo" class="form-control" oninput="soloLetras(this)" value="" required placeholder="Ingrese un titulo">
  <br>
  <br>
  <label for=""><b>resumen </b></label>
  <input type="text" name="resumen" id="resumen" class="form-control" oninput="soloLetras(this)" value="" required placeholder="Ingrese un resumen">
  <br>
    <br>
    <label for=""><b>FECHA DE PUBLICACION: </b></label>
<input type="date" name="fecha_publicacion" id="fecha_publicacion" class="form-control" value="" required placeholder="Ingrese la fecha de publicacion">
<br>


  <label for=""><b>FOTO: </b></label>
  <input type="file" name="apellido" id="apellido" class="form-control" oninput="soloLetras(this)" value="" required placeholder="Seleeccione Foto">

  <br>
  
  <label for="id"><b>Revista: </b></label>
<select name="revista_id" id="revista_id" class="form-control" required>
    <option value="">Seleccione la Revista</option>
    <?php foreach ($listadoRevistas as $revista) : ?>
        <?php $selected = ($revista->id == $selectedRevistaid) ? 'selected' : ''; ?>
        <option value="<?php echo $revista->id; ?>" <?php echo $selected; ?>><?php echo $revista->nombre; ?></option>
    <?php endforeach; ?>
</select>
      <br>
      <label for="id"><b>Volumen: </b></label>
<select name="volumen_id" id="volumen_id" class="form-control" required>
    <option value="">Seleccione la Revista</option>
    <?php foreach ($listadoVolumenes as $volumen) : ?>
        <?php $selected = ($volumen->id == $selectedVolumenid) ? 'selected' : ''; ?>
        <option value="<?php echo $volumen->id; ?>" <?php echo $selected; ?>><?php echo $volumen->nombre; ?></option>
    <?php endforeach; ?>
</select>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">
        <i class="fa fa-cloud-upload fa-fade" aria-hidden="true"></i>
        Guardar
      </button>
      <a href="<?php echo site_url('articulos/index'); ?>" class="btn btn-danger" >
        <i class="fa-solid fa-ban fa-fade"  > </i>
        Cancelar
      </a>
    </div>
  </div>

  <br><br>

</form>

<script type="text/javascript">
        $("#frm_nuevo_articulo").validate({
      errorClass: 'text-danger',
      rules: {
          "id": {
              required: true
          },
          "nombre": {
              required: true,
              minlength: 3,
              maxlength: 50
          },
          "apellido": {
              required: true,
              minlength: 3,
              maxlength: 50
          },
          "institucion": {
              required: true,
              minlength: 3,
              maxlength: 50
          }
      },
      messages: {
          "id": {
              required: '<span class="text-danger">Por favor, seleccione la revista</span>'
          },
          "nombre": {
              required: '<span class="text-danger">Debe ingresar el nombre</span>',
              minlength: '<span class="text-danger">El nombre debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">El nombre debe tener como máximo 50 caracteres</span>'
          },
          "apellido": {
              required: '<span class="text-danger">Debe ingresar el apellido exacto</span>',
              minlength: '<span class="text-danger">El apellido debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">El apellido debe tener como máximo 50 caracteres</span>'
          },
          "institucion": {
              required: '<span class="text-danger">Debe ingresar la institución de donde viene</span>',
              minlength: '<span class="text-danger">La institución debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">La institución debe tener como máximo 50 caracteres</span>'
          }
      }
      });

</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
