<?php
class Colaboradores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Colaborador");

        // Disable PHP errors and warnings
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoColaboradores"] = $this->Colaborador->consultarTodos();
        $this->load->view("header");
        $this->load->view("colaboradores/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idColaborador)
    {
        $this->Editorial->eliminar($idColaborador);
        $this->session->set_flashdata("confirmacion", "Colaborador eliminada correctamente");

        redirect("colaboradores/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("colaboradores/nuevo");
        $this->load->view("footer");
    }

    public function guardarColaborador()
    {
        $datosNuevoColaborador = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "institucion" => $this->input->post("institucion"),
            "articulo" => $this->input->post("articulo"),
            "revista" => $this->input->post("revista")

        );

        $this->Colaborador->insertar($datosNuevoColaborador);
        $this->session->set_flashdata("confirmacion", "Colaborador guardado exitosamente");
        redirect('colaboradores/index');
    }

    public function editar($idColaborador)
    {
        $data["colaboradorEditar"] = $this->Colaborador->obtenerPorId($idColaborador);
        $this->load->view("header");
        $this->load->view("colaboradores/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarColaborador()
    {
        $idColaborador = $this->input->post("idColaborador");

        $datosColaborador = array(
            "nombre" => $this->input->post("nombre"),
            "apellido" => $this->input->post("apellido"),
            "institucion" => $this->input->post("institucion"),
            "articulo" => $this->input->post("articulo"),
            "revista" => $this->input->post("revista")   
        );

        $this->Colaborador->actualizar($idColaborador, $datosColaborador);
        $this->session->set_flashdata("confirmacion", "Colaborador actualizado exitosamente");
        redirect('colaboradores/index');
    }

}
?>
