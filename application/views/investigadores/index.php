<div class="row">
    <div class="col-md-12 text-end">
        <!-- Botón para agregar un nuevo investigador -->
        <a href="<?php echo site_url('investigadores/nuevo'); ?>" class="btn btn-outline-success">
            <i class="fa fa-plus-circle fa-1x"></i>
            Agregar Investigador
        </a>
        <br><br>
    </div>
</div>

<div class="row">
    <?php if ($listadoInvestigadores): ?>
        <table class="table table-bordered" id="tbl_investigadores">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NOMBRE</th>
                    <th>APELLIDO</th>
                    <th>INSTITUCION</th>
                    <th>FOTO</th>
                    <th>TELEFONO</th>
                    <th>IDENTIFICACION</th>
                    <th>ACCIONES</th> <!-- Añadido para las acciones -->
                </tr>
            </thead>
            <tbody>
                <?php foreach ($listadoInvestigadores as $investigador): ?>
                    <tr>
                        <td><?php echo $investigador->id; ?></td>
                        <td><?php echo $investigador->nombre; ?></td>
                        <td><?php echo $investigador->apellido; ?></td>
                        <td><?php echo $investigador->institucion; ?></td>
                        <td>
                <?php if (!empty($investigador->foto)) : ?>
                    <img src="<?php echo base_url('ruta/de/tu/carpeta/fotos/') . $investigador->foto; ?>" alt="Foto de <?php echo $investigador->nombre; ?>" width="100">
                <?php else: ?>
                    Sin foto
                <?php endif; ?>
            </td>
                        <td><?php echo $investigador->telefono; ?></td>
                        <td><?php echo $investigador->CI; ?></td>
                        <td>
                            <!-- Acciones para editar y borrar investigadores -->
                            <a href="<?php echo site_url('investigadores/editar/') . $investigador->id; ?>" class="btn btn-warning" title="Editar">
                                <i class="fa fa-pen"></i>
                            </a>
                            <a href="<?php echo site_url('investigadores/borrar/') . $investigador->id; ?>" class="btn btn-danger" title="Eliminar" onclick="return confirm('¿Seguro desea eliminar este registro?');">
                            <i class="fa fa-trash"></i>
                        </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <div class="alert alert-danger">
            No se encontraron investigadores registrados.
        </div>
    <?php endif; ?>
</div>

<!-- Modal -->
<script type="text/javascript">
    function borrar(url) {
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar el investigador seleccionado?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href = url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }],
            ]
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tbl_investigadores').DataTable({
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdfHtml5',
                    messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
                },
                'print',
                'csv'
            ]
        });
    });
</script>
