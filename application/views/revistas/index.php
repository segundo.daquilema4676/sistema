<h1>
  <i class="fa fa-book"></i>
  REVISTAS
</h1>

<div class="row">
  <div class="col-md-12 text-end">

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver mapa
    </button>

    <a href="<?php echo site_url('revistas/nueva');?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i>
      AGREGAR REVISTA
    </a>
    <br><br>
  </div>
</div>

<?php if ($listadoRevistas): ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>NOMBRE</th>
      <th>AUTOR</th>
      <th>TIPO PUBLICACIÓN</th>
      <th>RESUMEN</th>
      <th>PALABRAS CLAVE</th>
      <th>ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoRevistas as $revista): ?>
    <tr>
      <td><?php echo $revista->id; ?></td>
      <td><?php echo $revista->nombre; ?></td>
      <td><?php echo $revista->autor; ?></td>
      <td><?php echo $revista->tipo_publicacion; ?></td>
      <td><?php echo $revista->resumen; ?></td>
      <td><?php echo $revista->palabras_clave; ?></td>
      <td>
        <a href="<?php echo site_url('revistas/editar/') . $revista->id; ?>" class="btn btn-warning" title="Editar">
          <i class="fa fa-pen"></i>
          Editar
        </a>
        <a href="<?php echo site_url('revistas/borrar/') . $revista->id; ?>" class="btn btn-danger" title="Eliminar">
          <i class="fa fa-trash"></i>
          Eliminar
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          <i class="fa fa-eye"></i> MAPA DE REVISTAS
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </h5>
      </div>
      <div class="modal-body">
        <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;">
          <script type="text/javascript">
            function initMap(){
              var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
              var miMapa = new google.maps.Map(
                document.getElementById('reporteMapa'),
                {
                  center: coordenadaCentral,
                  zoom: 8,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                }
              );
              // Marcadores para revistas
              <?php foreach ($listadoRevistas as $revista): ?>
                var coordenadaTemporal = new google.maps.LatLng(<?php echo $revista->latitud; ?>, <?php echo $revista->longitud; ?>);
                var marcador = new google.maps.Marker({
                  position: coordenadaTemporal,
                  map: miMapa,
                  title: '<?php echo $revista->nombre; ?>'
                });
              <?php endforeach; ?>
            }
          </script>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<?php else: ?>
<div class="alert alert-danger">
  No se encontraron revistas registradas
</div>
<?php endif; ?>
