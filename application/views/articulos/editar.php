<h1>EDITAR ARTICULO</h1>

<form class="" method="post" action="<?php echo site_url('articulos/actualizar'); ?>" id="frm_nuevo_articulo">
	<input type="hidden" name="id" id="id" value="<?php echo $articuloEditar->id; ?>">
  
    <label for=""><b>TITULO: </b></label>
    <input type="text" name="titulo" id="titulo" class="form-control" value="<?php echo $articuloEditar->titulo; ?>" required placeholder="Ingrese un título">
    <br>
    <br>
  
    <label for=""><b>RESUMEN: </b></label>
    <input type="text" name="resumen" id="resumen" class="form-control" value="<?php echo $articuloEditar->resumen; ?>" required placeholder="Ingrese un resumen">
    <br>
    <br>

    <label for=""><b>FECHA DE PUBLICACION: </b></label>
    <input type="date" name="fecha_publicacion" id="fecha_publicacion" class="form-control" value="<?php echo $articuloEditar->fecha_publicacion; ?>" required placeholder="Ingrese la fecha de publicación">
    <br>
    <br>

<<<<<<< HEAD
   <br>
=======
    <label for=""><b>FOTO: </b></label>
    <input type="file" name="foto" id="foto" class="form-control" required placeholder="Seleccione una foto">
    <br>

    <label for=""><b>Revista: </b></label>
    <select name="revista_id" id="revista_id" class="form-control" required>
        <option value="">Seleccione la Revista</option>
        <?php foreach ($listadoRevistas as $revista) : ?>
            <option value="<?php echo $revista->id; ?>" <?php echo ($revista->id == $articuloEditar->revista_id) ? 'selected' : ''; ?>><?php echo $revista->nombre; ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <label for=""><b>Volumen: </b></label>
    <select name="volumen_id" id="volumen_id" class="form-control" required>
        <option value="">Seleccione el Volumen</option>
        <?php foreach ($listadoVolumenes as $volumen) : ?>
            <option value="<?php echo $volumen->id; ?>" <?php echo ($volumen->id == $articuloEditar->volumen_id) ? 'selected' : ''; ?>><?php echo $volumen->nombre; ?></option>
        <?php endforeach; ?>
    </select>
    <br>
>>>>>>> 2202e1bfb030d3ee8580068ca037ba629f897634

    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
                <i class="fa fa-cloud-upload fa-fade" aria-hidden="true"></i>
                Guardar
            </button>
            <a href="<?php echo site_url('articulos/index'); ?>" class="btn btn-danger">
                <i class="fa fa-times fa-spin" aria-hidden="true"></i>
                Cancelar
            </a>
        </div>
    </div>
</form>

<br>
<br>

<script type="text/javascript">
    $("#frm_nuevo_articulo").validate({
        errorClass: 'text-danger',
        rules: {
            "titulo": {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            "resumen": {
                required: true,
                minlength: 10,
                maxlength: 255
            },
            "fecha_publicacion": {
                required: true
            },
            "foto": {
                required: true
            },
            "revista_id": {
                required: true
            },
            "volumen_id": {
                required: true
            }
        },
        messages: {
            "titulo": {
                required: '<span class="text-danger">Debe ingresar el título</span>',
                minlength: '<span class="text-danger">El título debe tener al menos 3 caracteres</span>',
                maxlength: '<span class="text-danger">El título debe tener como máximo 100 caracteres</span>'
            },
            "resumen": {
                required: '<span class="text-danger">Debe ingresar el resumen</span>',
                minlength: '<span class="text-danger">El resumen debe tener al menos 10 caracteres</span>',
                maxlength: '<span class="text-danger">El resumen debe tener como máximo 255 caracteres</span>'
            },
            "fecha_publicacion": {
                required: '<span class="text-danger">Debe ingresar la fecha de publicación</span>'
            },
            "foto": {
                required: '<span class="text-danger">Debe seleccionar una foto</span>'
            },
            "revista_id": {
                required: '<span class="text-danger">Debe seleccionar una revista</span>'
            },
            "volumen_id": {
                required: '<span class="text-danger">Debe seleccionar un volumen</span>'
            }
        }
    });

</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
<<<<<<< HEAD

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
=======
>>>>>>> 2202e1bfb030d3ee8580068ca037ba629f897634
