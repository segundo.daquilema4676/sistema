<h1>EDITAR VOLUMEN</h1>
<form class="" method="post" action="<?php echo site_url('volumenes/actualizarvolumen'); ?>" id="frm_nuevo_volumen">
	<input type="hidden" name="id" id="id" value="<?php echo $volumenEditar->id; ?>">
	<label for=""><b>TIPO REVISTA: </b></label>
<select data-live-search="true" data-live-search-style="startsWith" class="selectpicker form-control" name="id" id="id" required>
    <option value="">Seleccione la Revista</option>
    <?php foreach ($listadoRevistas as $revista) : ?>
        <?php if ($revista->id_ == $volumenEditar->id) : ?>
            <option value="<?php echo $revista->id; ?>" selected><?php echo $revista->nombre; ?></option>
        <?php else: ?>
            <option value="<?php echo $revista->id; ?>"><?php echo $revista->nombre; ?></option>
        <?php endif; ?>
    <?php endforeach; ?>
</select>

<br>
  <label for="">
    <b>Titulo:</b>
  </label>
  <input type="text" name="titulo" id="titulo"
	value="<?php echo $volumenEditar->titulo; ?>"
  placeholder="Ingrese el titulo..." oninput="soloLetras(this)"class="form-control" required>
  <br>
  <label for="">
    <b>Fechad de publicación:</b>
  </label>
  <input type="text" name="fecha_publicacion" id="fecha_publicacion"
	value="<?php echo $volumenEditar->telefono; ?>"
  placeholder="Ingrese la fechad de publicación..." oninput="soloNumeros(this)" class="form-control" required>

    <br>

    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('volumenes/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-
spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>

<script type="text/javascript">
        $("#frm_nuevo_volumen").validate({
      errorClass: 'text-danger',
      rules: {
          "id": {
              required: true
          },
          "titulo": {
              required: true,
              minlength: 3,
              maxlength: 50
          },
          "telefono": {
              required: true,
              minlength: 7,
              maxlength: 8,
              digits: true // Esto asegura que solo se permitan dígitos
          }
      },
      messages: {
          "id": {
              required: '<span class="text-danger">Por favor, seleccione el banco</span>'
          },
          "titulo": {
              required: '<span class="text-danger">Debe ingresar el titulo</span>',
              minlength: '<span class="text-danger">El titulo debe tener al menos 3 caracteres</span>',
              maxlength: '<span class="text-danger">El titulo debe tener como máximo 50 caracteres</span>'
          },
          "fecha_publicacion": {
              required: '<span class="text-danger">Debe ingresar la fechad e publicacion</span>',
              minlength: '<span class="text-danger">La fecha de publicacion debe tener al menos 7 dígitos</span>',
              maxlength: '<span class="text-danger">La fecha de publicacion debe tener como máximo 8 dígitos</span>',
              digits: '<span class="text-danger">Por favor ingrese solo dígitos</span>'
          }
      }
      });

</script>

<script type="text/javascript">
    function soloNumeros(input) {
        // Reemplaza cualquier carácter que no sea un dígito numérico o un espacio con una cadena vacía
        input.value = input.value.replace(/[^\d\s]/g, '');
    }
</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>
