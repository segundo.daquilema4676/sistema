<div class="site-blocks-cover overlay" style="background-image: url(https://www.coopdaquilema.com/wp-content/uploads/2020/09/giros1.svg);" data-aos="fade" id="home-section">
  <a href="#next" class="mouse smoothscroll">
    <span class="mouse-icon">
      <span class="mouse-wheel"></span>
    </span>
  </a>
</div>

<div class="site-section" id="next">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md-4 text-center" data-aos="fade-up" data-aos-delay="">
        <img src="<?php echo base_url('static/img/1.jpg'); ?>" alt="Revista Científica" class="img-fluid w-75 mb-12">
        <br>
        <br>
        <h3 class="card-title">Artículos de Investigación</h3>

        <br>
        <p>Publica tus investigaciones en nuestra revista científica y contribuye al avance del conocimiento en tu campo.</p>
      </div>
      <div class="col-md-4 text-center" data-aos="fade-up" data-aos-delay="100">
        <img src="<?php echo base_url('static/img/a.jpg'); ?>" alt="Revista Científica" class="img-fluid w-75 mb-12">
        <br>
        <br>
        <h3 class="card-title">Acceso en Línea</h3>
        <br>
        <p>Accede a nuestra revista en línea y mantente al día con los últimos avances científicos desde cualquier lugar.</p>
      </div>
      <div class="col-md-4 text-center" data-aos="fade-up" data-aos-delay="200">
        <img src="<?php echo base_url('static/img/images.jpg'); ?>" alt="Revista Científica" class="img-fluid w-75 mb-12">
        <br>
        <br>
        <br>
        <h3 class="card-title">Suscripción</h3>
        <br>
        <p>Suscríbete a nuestra revista y obtén acceso exclusivo a contenido científico de alta calidad.</p>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="">
        <figure class="circle-bg">
          <img src="<?php echo base_url('static/img/1.png'); ?>" alt="Revista Científica" class="img-fluid w-100 mb-50">
        </figure>
      </div>
      <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="100">
        <div class="mb-4">
          <h3 class="h3 mb-4 text-black">Publicaciones Destacadas</h3>
          <p>Descubre nuestras publicaciones destacadas y explora investigaciones relevantes en tu campo de interés.</p>
        </div>

        <div class="mb-4">
          <ul class="list-unstyled ul-check success">
            <li>Artículos revisados por pares</li>
            <li>Acceso abierto a la biblioteca digital</li>
            <li>Notificaciones de nuevos lanzamientos</li>
          </ul>
        </div>

        <div class="mb-4">
          <form action="#">
            <div class="form-group d-flex align-items-center">
              <input type="text" class="form-control mr-2" placeholder="Ingresa tu correo electrónico">
              <input type="submit" class="btn btn-primary" value="Suscribirse">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
