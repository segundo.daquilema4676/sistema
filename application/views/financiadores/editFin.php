<h1>EDITAR FINANCIADOR</h1>
<form class="" method="post" action="<?php echo site_url('financiadores/actualizarFin'); ?>" id="frm_editar_financiador">
    <input type="hidden" name="id" id="id_editar" value="<?php echo $financiadorEditar->id; ?>">
    <div>
        <label for="nombre"><b>Nombre del Financiador: </b></label>
        <input type="text" name="nombre" id="nombre" value="<?php echo $financiadorEditar->nombre; ?>" class="form-control" required placeholder="Ingrese el nombre del financiador">
    </div>

    <div>
        <label for="revista_id"><b>Revista: </b></label>
        <select class="form-control" name="revista_id" id="revista_id" required>
            <option value="">Seleccione la Revista</option>
            <?php foreach ($listadoRevistas as $revista) : ?>
                <?php if ($revista->id == $financiadorEditar->revista_id) : ?>
                    <option value="<?php echo $revista->id; ?>" selected><?php echo $revista->nombre; ?></option>
                <?php else : ?>
                    <option value="<?php echo $revista->id; ?>"><?php echo $revista->nombre; ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
    </div>

    <br><br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
            <a href="<?php echo site_url('financiadores/indexFin'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>
        </div>
    </div>
</form>

<script type="text/javascript">
    $("#frm_editar_financiador").validate({
        errorClass: 'text-danger',
        rules: {
            "revista_id": {
                required: true
            },
            "nombre": {
                required: true,
                minlength: 3,
                maxlength: 50
            },
        },
        messages: {
            "revista_id": {
                required: '<span class="text-danger">Por favor, seleccione la revista</span>'
            },
            "nombre": {
                required: '<span class="text-danger">Debe ingresar el nombre</span>',
                minlength: '<span class="text-danger">El nombre debe tener al menos 3 caracteres</span>',
                maxlength: '<span class="text-danger">El nombre debe tener como máximo 50 caracteres</span>'
            },
        }
    });
</script>
