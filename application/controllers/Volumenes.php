<?php
class Volumenes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Volumen");
        $this->load->model("Revista");
    }

    public function index()
    {
        // Obtener todos los articulos
        $data['listadoRevistas'] = $this->Revista->consultarTodos();

        // Obtener todos los Volumenes
        $data["listadoVolumenes"] = $this->Volumen->consultarTodos();

        $this->load->view("header");
        $this->load->view("volumenes/index", $data);
        $this->load->view("footer");
    }

    // Eliminacion de Volumenes
    public function borrar($id)
    {
        $this->Volumen->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Volumen eliminado exitosamente");
        redirect("volumenes/index");
    }

    // Renderizacion del formulario de nuevo volumen
    public function nuevo()
    {
        // Obtener todos los articulos
        $data['listadoRevistas'] = $this->Revista->consultarTodos();

        $this->load->view("header");
        $this->load->view("volumenes/nuevo", $data);
        $this->load->view("footer");
    }

    // Capturando datos e insertando un nuevo volumen
    public function guardarVolumen()
    {
        $id = $this->input->post("id");

        $datosNuevoVolumen = array(
            "id" => $id,
            "titulo" => $this->input->post("titulo"),
            "fecha_publicacion" => $this->input->post("fecha_publicacion")
        );

        $this->Volumen->insertar($datosNuevoVolumen);
        $this->session->set_flashdata("confirmacion", "Volumen guardado exitosamente");
        redirect('volumenes/index');
    }

    // Renderizar el formulario de edicion
    // Renderizar el formulario de edicion
      public function editar($id)
      {
          // Obtener los datos del corresponsal a editar
          $data["volumenEditar"] = $this->Volumen->obtenerPorId($id);

          // Obtener todos las agencias
          $data['listadoRevistas'] = $this->Revista->consultarTodos();

          // Cargar la vista de edición con los datos del corresponsal y las agencias
          $this->load->view("header");
          $this->load->view("volumenes/editar", $data);
          $this->load->view("footer");
      }
    // Actualizar datos de volumen
    public function actualizarVolumen()
    {
        $id = $this->input->post("id");
        $datosVolumen = array(
          "titulo" => $this->input->post("titulo"),
          "fecha_publicacion" => $this->input->post("fecha_publicacion")
        );

        $this->Volumen->actualizar($id, $datosVolumen);
        $this->session->set_flashdata("confirmacion", "Volumen actualizado exitosamente");
        redirect('volumenes/index');
    }
}
?>
