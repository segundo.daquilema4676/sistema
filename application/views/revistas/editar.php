<div class="container">
  <h1 class="mb-4">Editar Revista</h1>
  <form method="post" action="<?php echo site_url('revistas/actualizarRevista'); ?>" enctype="multipart/form-data" id="formulario_revista">
    <input type="hidden" name="id" id="id" value="<?php echo $revistaEditar->id; ?>">

    <div class="form-group">
      <label for="nombre"><b>Nombre:</b></label>
      <input type="text" name="nombre" id="nombre" value="<?php echo $revistaEditar->nombre; ?>" placeholder="Ingrese el nombre..." class="form-control" required>
    </div>

    <div class="form-group">
      <label for="autor"><b>Autor:</b></label>
      <input type="text" name="autor" id="autor" value="<?php echo $revistaEditar->autor; ?>" placeholder="Ingrese el autor..." class="form-control" required>
    </div>

    <div class="form-group">
      <label for="tipo_publicacion"><b>Tipo de publicación:</b></label>
      <input type="text" name="tipo_publicacion" id="tipo_publicacion" value="<?php echo $revistaEditar->tipo_publicacion; ?>" placeholder="Ingrese el tipo de publicación..." class="form-control" required>
    </div>

    <div class="form-group">
      <label for="resumen"><b>Resumen:</b></label>
      <textarea name="resumen" id="resumen" class="form-control" rows="5" placeholder="Ingrese el resumen..." required><?php echo $revistaEditar->resumen; ?></textarea>
    </div>

    <div class="form-group">
      <label for="palabras_clave"><b>Palabras clave:</b></label>
      <textarea name="palabras_clave" id="palabras_clave" class="form-control" rows="3" placeholder="Ingrese las palabras clave..." required><?php echo $revistaEditar->palabras_clave; ?></textarea>
    </div>



    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button>
        <a href="<?php echo site_url('revistas/index'); ?>" class="btn btn-danger"> <i class="fa fa-times"></i> &nbsp Cancelar</a>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
  $("#formulario_revista").validate({
    rules: {
      "nombre": {
        required: true,
        minlength: 2,
        maxlength: 100
      },
      "autor": {
        required: true,
        minlength: 2,
        maxlength: 100
      },
      "tipo_publicacion": {
        required: true,
        minlength: 2,
        maxlength: 50
      },
      "resumen": {
        required: true
      },
      "palabras_clave": {
        required: true
      }
    },
    messages: {
      "nombre": {
        required: "Por favor, ingrese el nombre",
        minlength: "El nombre debe tener al menos 2 caracteres",
        maxlength: "El nombre no puede tener más de 100 caracteres"
      },
      "autor": {
        required: "Por favor, ingrese el autor",
        minlength: "El autor debe tener al menos 2 caracteres",
        maxlength: "El autor no puede tener más de 100 caracteres"
      },
      "tipo_publicacion": {
        required: "Por favor, ingrese el tipo de publicación",
        minlength: "El tipo de publicación debe tener al menos 2 caracteres",
        maxlength: "El tipo de publicación no puede tener más de 50 caracteres"
      },
      "resumen": {
        required: "Por favor, ingrese el resumen"
      },
      "palabras_clave": {
        required: "Por favor, ingrese las palabras clave"
      }
    },
    errorClass: "text-danger"
  });
</script>
