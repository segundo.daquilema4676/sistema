<?php
class Volumen extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Insertar nuevos
    function insertar($datos)
    {
        $respuesta = $this->db->insert("volumen", $datos);
        return $respuesta;
    }

    // Consulta de todos
    function consultarTodos()
    {
        $volumenes = $this->db->get("volumen");
        if ($volumenes->num_rows() > 0) {
            return $volumenes->result();
        } else {
            return false;
        }
    }

    // Eliminar por ID
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("volumen");
    }

    // Consultar un solo dato por ID
    function obtenerPorId($id)
    {
        $this->db->where("id", $id);
        $volumen = $this->db->get("volumen");
        if ($volumen->num_rows() > 0) {
            return $volumen->row();
        } else {
            return false;
        }
    }

    // Actualizar datos
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("volumen", $datos);
    }

    // Obtener revista
    function obtenerRevista()
    {
        $rivistas = $this->db->get("revista")->result();
        return $revistas;
    }
}
?>
