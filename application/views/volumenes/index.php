<br>
<i class="fas fa-revista fa-2x"> Volumenes</i>
<div class="row">
  <div class="col-md-12 text-end">
    <!-- Button trigger modal -->

    <a href="<?php echo site_url('volumenes/nuevo'); ?>"class="btn btn-outline-success">
      <i class="fa fa-plus-circle fa-1x"></i>
      Agregar Volumen
    </a>
    <br><br>
  </div>
  <!-- Google Maps API -->
</div>
<?php if ($listadoVolumenes): ?>
    <table class="table table-bordered" id="tbl_volumenes">
        <thead>
              <tr>
                <th>ID</th>
                <th>TITULO</th>
                <th>FECHA DE PUBLICACION</th>
                <th>REVISTA</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoVolumenes as $volumen): ?>
                <tr>
                  <td><?php echo $volumen->id; ?></td>
                  <td><?php echo $volumen->titulo; ?></td>
                  <td><?php echo $volumen->fecha_publicacion; ?></td>
                  <td><?php echo $volumen->revista_id; ?></td>
                  <td>
                      <?php
                      // Obtener el nombre de la revista correspondiente al id del volumen
                      $nombre_revista = '';
                      foreach ($listadoRevistas as $revista) {
                          if ($volumen->id == $revista->id) {
                              $nombre_revista = $revista->nombre;
                              break;
                          }
                      }
                      echo $nombre_revista;
                      ?>
                  </td>

                  <td>

                    <a href="<?php echo site_url('volumenes/editar/').$volumen->id; ?>"
                        class="btn btn-warning"
                        title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="javascript:void(0);" onclick="borrar('<?php echo site_url('volumenes/borrar/') . $volumen->id; ?>')"
                       class="btn btn-danger"
                       title="Borrar">
                       <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>



<!-- Modal -->

<script type="text/javascript">
        function borrar(url){
          iziToast.question({
              timeout: 15000,
              close: true,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Está seguro de eliminar el volumen seleccionado?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {
                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=url;
                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
        }
      </script>


<?php else: ?>
  <div class="alert alert-danger">
      No se encontro volumenes registrados
  </div>
<?php endif; ?>
<script type="text/javascript" >
    $('#tbl_volumenes').DataTable( {
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
            },
            'print',
            'csv'
        ]
    } );
</script>
