<?php
class Articulos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Articulo");
        $this->load->model("Revista");
        $this->load->model("Volumen");

    }

    public function index()
    {
        $data['listadoRevistas'] = $this->Revista->consultarTodos();
        $data['listadoVolumenes'] = $this->Volumen->consultarTodos();


        $data["listadoArticulos"] = $this->Articulo->consultarTodos();

        $this->load->view("header");
        $this->load->view("articulos/index", $data);
        $this->load->view("footer");
    }

    // Eliminacion de Articulo
    public function borrar($id)
    {
        $this->Articulo->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Articulo eliminado exitosamente");
<<<<<<< HEAD
        redirect("Articulos/index");
=======
        redirect("articulos/index");
>>>>>>> 2202e1bfb030d3ee8580068ca037ba629f897634
    }

    // Renderizacion del formulario de nuevo
    public function nuevo()
    {

      $data["listadoArticulos"] = $this->Articulo->consultarTodos();

        $data['listadoRevistas'] = $this->Revista->consultarTodos();
        $data['listadoVolumenes'] = $this->Volumen->consultarTodos();

        $this->load->view("header");
        $this->load->view("articulos/nuevo", $data);
        $this->load->view("footer");
    }

    // Capturando datos e insertando un nuevo Articulo
    public function guardar()
    {
        $id = $this->input->post("id");

        $datosNuevoArticulo = array(
            "titulo" => $this->input->post("titulo"),
            "resumen" => $this->input->post("resumen"),
            "fecha_publicacion" => $this->input->post("fecha_publicacion"),
            "foto" => $this->input->post("foto"),
        );

        $this->Articulo->insertar($datosNuevoArticulo);
        $this->session->set_flashdata("confirmacion", "Articulo guardado exitosamente");
        redirect('articulos/index');
    }

      public function editar($id)
      {
          // Obtener los datos del corresponsal a editar
          $data["articuloEditar"] = $this->Articulo->obtenerPorId($id);
          $data['listadoRevistas'] = $this->Revista->consultarTodos();
          $data['listadoVolumenes'] = $this->Volumen->consultarTodos();

          $this->load->view("header");
          $this->load->view("articulos/editar", $data);
          $this->load->view("footer");
      }

    public function actualizar()
    {
        $id = $this->input->post("id");

        $datosArticulo = array(

            "titulo" => $this->input->post("titulo"),
            "resumen" => $this->input->post("resumen"),
            "fecha_publicacion" => $this->input->post("fecha_publicacion"),
            "foto" => $this->input->post("foto"),

        );

        $this->Articulo->actualizar($id, $datosArticulo);
        $this->session->set_flashdata("confirmacion", "Articulo actualizado exitosamente");
        redirect('articulos/index');
    }
}
?>
