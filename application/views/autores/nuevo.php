<div class="container">
  <h1>
    <b>
      <i class="fa fa-plus-circle"></i>
      NUEVO AUTOR
    </b>
  </h1>
  <br>
  <div class="row">
    <div class="col-md-6">
      <form action="<?php echo site_url('autores/guardarAutor');?>" method="post" enctype="multipart/form-data" id="formulario_autor">

        <div class="form-group">
          <label for=""><b>INVESTIGADOR:</b></label>
          <br>
          <select name="investigador_id" id="investigador_id" class="form-control" required>
              <?php foreach ($investigadores as $investigador) : ?>
                  <option value="<?php echo $investigador->id; ?>"><?php echo $investigador->nombre; ?></option>
              <?php endforeach; ?>
          </select>
        </div>

        <div class="form-group">
          <label for=""><b>ARTICULO:</b></label>
          <br>
          <select name="articulo_id" id="articulo_id" class="form-control" required>
              <?php foreach ($articulos as $articulo) : ?>
                  <option value="<?php echo $articulo->id; ?>"><?php echo $articulo->titulo; ?></option>
              <?php endforeach; ?>
          </select>
        </div>

        <div class="form-group">
          <button type="submit" name="button" class="btn btn-primary"> <i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>
          <a href="<?php echo site_url('autores/index');?>" class="btn btn-danger"> <i class="fa-solid fa-ban"></i> CANCELAR</a>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
    $("#formulario_autor").validate({
        rules:{
            "investigador_id": {
                required: true
            },
            "articulo_id": {
                required: true
            }
        },
        messages:{
            "investigador_id": {
                required: "Por favor, seleccione un investigador"
            },
            "articulo_id": {
                required: "Por favor, seleccione un artículo"
            }
        },
        errorClass: "text-danger" // Agregar esta línea para establecer la clase de estilo para los mensajes de error
    });
</script>
