<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Nuevo Investigador</title>
<!-- Agrega referencias a jQuery y jQuery Validate -->
</head>

<body>
  <h1>
    <br>
    <b>
      <i class="fa fa-plus-circle"></i>
      Nuevo Investigador
    </b>
  </h1>
  <form class="" action="<?php echo site_url('investigadores/guardar'); ?>" enctype="multipart/form-data" method="post" id="frm_nuevo_investigador">
  <br>
  <div class="row">
    <br>
    <div class="col-6">
      <label for=""><b>NOMBRE: </b></label>
      <input type="text" name="nombre" id="nombre" class="form-control" oninput="soloLetras(this)" value="" required
        placeholder="Ingrese su nombre">
    </div>
    <div class="col-6">
      <label for=""><b>APELLIDO: </b></label>
      <input type="text" name="apellido" id="apellido" class="form-control"
      oninput="soloLetras(this)" value="" required placeholder="Ingrese su apellido">
      <br>
    </div>
    <div class="col-4">
      <label for=""><b>INSTITUCION: </b></label>
      <input type="text" name="institucion" id="institucion" class="form-control"
      oninput="soloLetras(this)" value="" required placeholder="Ingrese el nombre de la institucion">
      <br>
    </div>
    <div class="col-4">
      <label for=""><b>TELEFONO: </b></label>
      <input type="number" name="telefono" id="telefono" class="form-control"
        oninput="soloNumeros(this)" value="" required placeholder="Ingrese el numero de telefono">
      <br>
    </div>
    <div class="col-4">
      <label for=""><b>IDENTIFICACION: </b></label>
      <input type="number" name="identificacion" id="identificacion" class="form-control"
        oninput="soloNumeros(this)" value="" required placeholder="Ingrese su identificacion">
      <br>
    </div>
    <div class="col-6">
      <label for=""><b>FOTO: </b></label>
      <input type="file" name="foto" id="foto" class="form-control"
        oninput="soloFotos(this)" value="" required placeholder="Ingrese la fecha de publicación">
      <br>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">
        <i class="fa fa-cloud-upload fa-fade" aria-hidden="true"></i>
        Guardar
      </button>
      <a href="<?php echo site_url('investigadores/index'); ?>" class="btn btn-danger">
        <i class="fa-solid fa-ban fa-fade"> </i>
        Cancelar
      </a>
    </div>
  </div>

  <br><br>

  </form>

  <script type="text/javascript">
    $("#frm_nuevo_investigador").validate({
      errorClass: 'text-danger',
      rules: {
        "id": {
          required: true
        },
        "titulo": {
          required: true,
          minlength: 3,
          maxlength: 50
        }
      },
      messages: {
        "id": {
          required: '<span class="text-danger">Por favor, seleccione la revista</span>'
        },
        "titulo": {
          required: '<span class="text-danger">Debe ingresar el nombre</span>',
          minlength: '<span class="text-danger">El nombre debe tener al menos 3 caracteres</span>',
          maxlength: '<span class="text-danger">El nombre debe tener como máximo 50 caracteres</span>'
        }
      }
    });

  </script>

  <script type="text/javascript">
    function soloNumeros(input) {
      // Reemplaza cualquier carácter que no sea un dígito numérico o un espacio con una cadena vacía
      input.value = input.value.replace(/[^\d\s]/g, '');
    }
  </script>

  <script type="text/javascript">
    function soloLetras(input) {
      // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
      input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
  </script>