<br>
<i class="fas fa-revista fa-2x"> Financiadores</i>
<div class="row">
    <div class="col-md-12 text-end">
        <!-- Button trigger modal -->

        <a href="<?php echo site_url('financiadores/nuevoFin'); ?>" class="btn btn-outline-success">
            <i class="fa fa-plus-circle fa-1x"></i>
            Agregar Financiador
        </a>

        <br><br>
    </div>
    <!-- Google Maps API -->
</div>
<?php if ($listadoFinanciadores) : ?>
    <table class="table table-bordered" id="tbl_financiadores">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE FINANCIADOR</th>
                <th>REVISTA</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoFinanciadores as $financiador) : ?>
                <tr>
                    <td><?php echo $financiador->id; ?></td>
                    <td><?php echo $financiador->nombre; ?></td>
                    <td><?php echo $financiador->revista_id; ?></td>
                    <td>
                        <?php
                        // Buscar el nombre de la revista correspondiente al ID del financiador
                        $nombre_revista = '';
                        foreach ($listadoRevistas as $revista) {
                            if ($financiador->revista_id == $revista->id) {
                                $nombre_revista = $revista->nombre;
                                break;
                            }
                        }
                        echo $nombre_revista;
                        ?>
                    </td>
                    <td>
                        <a href="<?php echo site_url('financiadores/editFin/') . $financiador->id; ?>" class="btn btn-warning" title="Editar">
                            <i class="fa fa-pen"></i>
                        </a>
                        <a href="<?php echo site_url('financiadores/borrar/') . $financiador->id; ?>" class="btn btn-danger" title="Eliminar" onclick="return confirm('¿Seguro desea eliminar este registro?');">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>




    <!-- Modal -->

    <script type="text/javascript">
        function borrar(url) {
            iziToast.question({
                timeout: 15000,
                close: true,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'CONFIRMACIÓN',
                message: '¿Está seguro de eliminar el financiador seleccionado?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function(instance, toast) {
                        instance.hide({
                            transitionOut: 'fadeOut'
                        }, toast, 'button');
                        window.location.href = url;
                    }, true],
                    ['<button>NO</button>', function(instance, toast) {

                        instance.hide({
                            transitionOut: 'fadeOut'
                        }, toast, 'button');

                    }],
                ]
            });
        }
    </script>


<?php else : ?>
    <div class="alert alert-danger">
        No se encontro volumenes registrados
    </div>
<?php endif; ?>
<script type="text/javascript">
    $('#tbl_financiadores').DataTable({
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [{
                extend: 'pdfHtml5',
                messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
            },
            'print',
            'csv'
        ]
    });
</script>