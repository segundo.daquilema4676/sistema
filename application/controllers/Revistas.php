<?php
class Revistas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Revista");

        // Deshabilitar errores y advertencias de PHP
        error_reporting(0);
    }

    public function index()
    {
        $data["listadoRevistas"] = $this->Revista->consultarTodos();
        $this->load->view("header");
        $this->load->view("revistas/index", $data);
        $this->load->view("footer");
    }

    public function borrar($idRevista)
    {
        $this->Revista->eliminar($idRevista);
        $this->session->set_flashdata("confirmacion", "Revista eliminada correctamente");

        redirect("revistas/index");
    }

    public function nueva()
    {
        $this->load->view("header");
        $this->load->view("revistas/nuevo");
        $this->load->view("footer");
    }

    public function guardarRevista()
    {
        $datosNuevaRevista = array(
            "nombre" => $this->input->post("nombre"),
            "autor" => $this->input->post("autor"),
            "tipo_publicacion" => $this->input->post("tipo_publicacion"),
            "resumen" => $this->input->post("resumen"),
            "palabras_clave" => $this->input->post("palabras_clave")
        );

        $this->Revista->insertar($datosNuevaRevista);
        $this->session->set_flashdata("confirmacion", "Revista guardada exitosamente");
        redirect('revistas/index');
    }

    public function editar($idRevista)
    {
        $data["revistaEditar"] = $this->Revista->obtenerPorId($idRevista);
        $this->load->view("header");
        $this->load->view("revistas/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarRevista()
    {
        $idRevista = $this->input->post("id");
        $datosRevista = array(
            "nombre" => $this->input->post("nombre"),
            "autor" => $this->input->post("autor"),
            "tipo_publicacion" => $this->input->post("tipo_publicacion"),
            "resumen" => $this->input->post("resumen"),
            "palabras_clave" => $this->input->post("palabras_clave")
        );

        $this->Revista->actualizar($idRevista, $datosRevista);
        $this->session->set_flashdata("confirmacion", "Revista actualizada exitosamente");
        redirect('revistas/index');
    }
}
?>
