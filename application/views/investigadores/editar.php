<h1>EDITAR INVESTIGADOR</h1>
<form class="" method="post" action="<?php echo site_url('investigadores/actualizar'); ?>" id="frm_nuevo_investigador">
    <input type="hidden" name="id" id="id" value="<?php echo $investigadorEditar->id; ?>">
    <div class="row">
        <br>
        <div class="col-6">
            <label for=""><b>NOMBRE: </b></label>
            <input type="text" name="nombre" id="nombre" class="form-control" oninput="soloLetras(this)" value="<?php echo $investigadorEditar->nombre; ?>" required placeholder="Ingrese su nombre">
        </div>
        <div class="col-6">
            <label for=""><b>APELLIDO: </b></label>
            <input type="text" name="apellido" id="apellido" class="form-control" oninput="soloLetras(this)" value="<?php echo $investigadorEditar->apellido; ?>" required placeholder="Ingrese su apellido">
            <br>
        </div>
        <div class="col-4">
            <label for=""><b>INSTITUCION: </b></label>
            <input type="text" name="institucion" id="institucion" class="form-control" oninput="soloLetras(this)" value="<?php echo $investigadorEditar->institucion; ?>" required placeholder="Ingrese el nombre de la institucion">
            <br>
        </div>
        <div class="col-4">
            <label for=""><b>TELEFONO: </b></label>
            <input type="number" name="telefono" id="telefono" class="form-control" oninput="soloNumeros(this)" value="<?php echo $investigadorEditar->telefono; ?>" required placeholder="Ingrese el numero de telefono">
            <br>
        </div>
        <div class="col-4">
            <label for=""><b>IDENTIFICACION: </b></label>
            <input type="number" name="CI" id="CI" class="form-control" oninput="soloNumeros(this)" value="<?php echo $investigadorEditar->CI; ?>" required placeholder="Ingrese su identificacion">
            <br>
        </div>
        <div class="col-6">
            <label for="foto"><b>FOTO: </b></label>
            <input type="file" name="foto" id="foto" class="form-control" oninput="soloFotos(this)" required placeholder="Ingrese una foto">
            <br>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
            <a href="<?php echo site_url('investigadores/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-
spin"></i> &nbsp Cancelar</a>

        </div>

    </div>
</form>

<br>
<br>

<script type="text/javascript">
    $("#frm_nuevo_investigador").validate({
        errorClass: 'text-danger',
        rules: {
            "id": {
                required: true
            },
            "nombre": {
                required: true,
                minlength: 3,
                maxlength: 50
            },
        },
        messages: {
            "id": {
                required: '<span class="text-danger">Por favor, seleccione el finanaciador</span>'
            },
            "nombre": {
                required: '<span class="text-danger">Debe ingresar el nombre</span>',
                minlength: '<span class="text-danger">El titulo debe tener al menos 3 caracteres</span>',
                maxlength: '<span class="text-danger">El titulo debe tener como máximo 50 caracteres</span>'
            },

        }
    });
</script>

<script type="text/javascript">
    function soloNumeros(input) {
        // Reemplaza cualquier carácter que no sea un dígito numérico o un espacio con una cadena vacía
        input.value = input.value.replace(/[^\d\s]/g, '');
    }
</script>

<script type="text/javascript">
    function soloLetras(input) {
        // Reemplaza cualquier carácter que no sea una letra o un espacio con una cadena vacía
        input.value = input.value.replace(/[^a-zA-Z\s]/g, '');
    }
</script>