<h1>EDITAR AUTOR</h1>
<form method="post" action="<?php echo site_url('autores/actualizarAutor'); ?>" enctype="multipart/form-data" id="formulario_autor">
  <input type="hidden" name="id" id="id" value="<?php echo $autorEditar->id; ?>">
  <label for=""><b>INVESTIGADOR:</b></label>
  <br>
  <select name="investigador_id" id="investigador_id" class="form-control" required>
      <?php foreach ($investigadores as $investigador) : ?>
          <option value="<?php echo $investigador->id; ?>" <?php echo ($investigador->id == $autorEditar->investigador_id) ? 'selected' : ''; ?>><?php echo $investigador->nombre; ?></option>
      <?php endforeach; ?>
  </select>
  <br>
  <label for=""><b>ARTICULO:</b></label>
  <br>
  <select name="articulo_id" id="articulo_id" class="form-control" required>
      <?php foreach ($articulos as $articulo) : ?>
          <option value="<?php echo $articulo->id; ?>" <?php echo ($articulo->id == $autorEditar->articulo_id) ? 'selected' : ''; ?>><?php echo $articulo->titulo; ?></option>
      <?php endforeach; ?>
  </select>
  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp ACTUALIZAR</button> &nbsp &nbsp
      <a href="<?php echo site_url('autores/index'); ?>" class="btn btn-danger"> <i class="fa fa-times"></i> &nbsp Cancelar</a>
    </div>
  </div>
</form>
